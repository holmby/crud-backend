<?php
namespace Holmby\CRUD;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Exception\HttpNotFoundException;
use \Slim\Exception\HttpForbiddenException;
use \PDO;

use \Holmby\Auth\Authenticate;
use \Holmby\CRUD\Config;

abstract class CRUD extends Database {
  protected $auth;
  protected $conf;

  /**
   * You must initialize the self referencing constants below when extending this class.
   * Associative arrays for mapping database names to rest-api names:
   * database-column-name => rest-api-name
   */
  const TABLE = 'TABLE not initialized in subclass to CRUD'; // self::TABLE;
  const COLUMNS = ['COLUMNS not initialized in subclass to CRUD']; // self::COLUMNS;
  const KEYS = array('id' => 'id');

  public function __construct(Authenticate $auth, Config $conf) {
      parent::__construct($conf);
      $this->auth = $auth;
  }

  /**
   * Check if the user have privileges for the operation.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function authorizeRead(Request $request, $args) {
    throw new HttpForbiddenException($request, 'unautorized read request');
  }
  public function authorizeReadAll(Request $request, $args) {
    throw new HttpForbiddenException($request, 'unautorized read request');
  }
  public function authorizeDelete(Request $request, $id) {
    throw new HttpForbiddenException($request, 'unautorized delete request');
  }
  public function authorizeCreate(Request $request) {
    throw new HttpForbiddenException($request, 'unautorized create request');
  }
  public function authorizeReplace(Request $request, $args) {
    throw new HttpForbiddenException($request, 'unautorized replace request');
  }

  /**
   * Execute a read operation, returns one element.
   * Calls authorizeRead() to autorize the read operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function read(Request $request, Response $response, $args) {
    $this->authorizeRead($request, $args);
    $selectNames = $this->makeSqlSelectPart($this::KEYS);
    if(!empty($this::KEYS) || !empty($this::COLUMNS)) {
      $selectNames .= ',';
    }
    $selectNames .= $this->makeSqlSelectPart($this::COLUMNS);
    $condition = $this->makeSqlNameBinding($this::KEYS, ' and ');
    $query = 'select ' . $selectNames
           . ' from ' . $this::TABLE
           . ' where ' . $condition;
    $pdo = $this->connect();
    $stm = $pdo->prepare($query);
    $this->bindValues($this::KEYS, $args, $stm);
    $stm->execute();
    $row = $stm->fetch();
    if(!$row) {
      throw new HttpNotFoundException($request, 'No matching person found.');
    }
    $payload = json_encode($row);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
  }

  /**
   * Execute a read operation, returns all elements.
   * Calls authorizeRead() to autorize the read operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function readAll(Request $request, Response $response, $args) {
    $this->authorizeReadAll($request, $args);
    $selectNames = $this->makeSqlSelectPart($this::KEYS);
    if(!empty($this::KEYS) || !empty($this::COLUMNS)) {
      $selectNames .= ',';
    }
    $selectNames .= $this->makeSqlSelectPart($this::COLUMNS);
    $query = 'select ' . $selectNames
           . ' from ' . $this::TABLE;
    $pdo = $this->connect();
    $stm = $pdo->prepare($query);
    $stm->execute();
    $result = $stm->fetchAll();
    $payload = json_encode($result);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
  }

  /**
   * Execute a delete operation.
   * Calls authorizeDelete() to autorize the operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function delete(Request $request, Response $response, $args) {
    $this->authorizeDelete($request, $args);
    $pdo = $this->connect();
    $keys = $this->makeSqlNameBinding($this::KEYS, ' and ');
		$query = 'delete from ' . $this::TABLE
           .  ' where ' . $keys;
    $stm = $pdo->prepare($query);
    $this->bindValues($this::KEYS, $args, $stm);
    $stm->execute();
    if($stm->rowCount() < 1) {
      throw new HttpNotFoundException($request, 'No matching item found.');
    }
    return $response->withHeader('Cache-control', 'no-store');
  }

  /**
   * Execute a create operation.
   * Calls authorizeCreate() to autorize the operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function create(Request $request, Response $response) {
    // TODO, check for duplicate key
    $this->authorizeCreate($request);
    $body = $request->getParsedBody();
    $pdo = $this->connect();
    $columns = implode(',', array_keys($this::COLUMNS));
    $values = implode(',:', array_keys($this::COLUMNS));
    // TODO, this assumes one autoincrement primary key
    $query = 'insert into ' . $this::TABLE . '(' . $columns . ') Values (:' . $values . ')';
    $stm = $pdo->prepare($query);
    foreach($this::COLUMNS as $db => $rest) {
      $stm->bindParam(':' . $db, $body[$rest], PDO::PARAM_STR);
    }
    $stm->execute();
    // TODO, this assumes one autoincrement primary key named 'id'
    $id = $pdo->lastInsertId();
    $args = array('id' => $id);
    return $this->read($request, $response, $args)->withHeader('Cache-control', 'no-store')->withStatus(201, 'Created');
  }
  /**
   * Execute a replace operation.
   * Calls authorizeReplace() to autorize the operation for the current user.
   * The primary key must be a single column.
   * @throws HttpForbiddenException if the user do not have permission for the operation
   */
  public function replace(Request $request, Response $response, $args) {
    $this->authorizeReplace($request, $args);
    $body = $request->getParsedBody();
    $pdo = $this->connect();
    $keys = $this->makeSqlNameBinding($this::KEYS, ' and ');
    $values = $this->makeSqlNameBinding($this::COLUMNS);
    $query = 'update ' . $this::TABLE . ' set ' . $values . ' where ' . $keys;
    // TODO, this do not update the primary keys
    $stm = $pdo->prepare($query);
    $this->bindValues($this::KEYS, $args, $stm);
    $this->bindValues($this::COLUMNS, $body, $stm);
    $stm->execute();
    // TODO, check if a row is updated, if not return 204 (No Content)
    // TODO, this assumes that the primary keys are not updated
    return $this->read($request, $response, $args)->withHeader('Cache-control', 'no-store');
  }

}
?>