<?php
namespace Holmby\CRUD;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Exception\HttpNotFoundException;
use \Slim\Exception\HttpUnauthorizedException;
use \PDO;

use \Holmby\Auth\Authenticate;
use \Holmby\CRUD\Config;

class Database {
  protected $conf;

  const OPTIONS = [
    PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
  ];

  public function __construct(Config $conf) {
    $this->conf = $conf;
  }

  /**
   * Connect to the database.
   */
  public function connect(): PDO {
    $dns = 'mysql:host=' . $this->conf::DB_HOST . ";dbname=" . $this->conf::DB_DATABASE . ";charset=utf8mb4";
    return new PDO($dns, $this->conf::DB_USER, $this->conf::DB_PWD, static::OPTIONS);
  }

  /**
   * Generates the column part of a sql select query.
   * sample output:
   * "db1 AS rest1, db2 AS rest2, db3 AS rest3"
   * @param $array an associative with database-column-names => rest-names
   * @return a string commasepareted database-column-names AS rest-names
   */
  public static function makeSqlSelectPart($array): string {
    $sep = '';
    $acc = '';
    foreach($array as $db => $rest) {
      $acc .= $sep . $db . ' AS ' . $rest;
      $sep = ',';
    }
    return $acc;
  }
  /**
   * Generate a string binding database-names to rest-api-names for a sql query.
   * This is usefull in the where par, and in an insert query.
   * sample output:
   * "db1 = :rest1, db2 = :rest2, db3 = :rest3"
   * @param $names an associative containing: database-column-name => rest-name
   * @return a string commasepareted database-column-names AS rest-names
   */
  public static function makeSqlNameBinding($names, $sep = ','): string {
    $sep2 = '';
    $acc = '';
    foreach($names as $db => $rest) {
      $acc .= $sep2 . $db . ' = :' . $rest;
      $sep2 = $sep;
    }
    return $acc;
  }

  /**
   * Binds values to names in a sql query generated by makeSqlNameBinding
   * @param $names an associative containing: database-column-name => rest-name
   * @param $values an associative array: $values[rest-name] => value
   * @param $stm the PDO statement containing the sql query
   */
  public function bindValues($names, $values, $stm) {
    foreach($names as $db => $rest) {
      $stm->bindParam(':' . $rest, $values[$rest], PDO::PARAM_STR);
    }
  }
}
?>