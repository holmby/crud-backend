<?php
namespace Holmby\CRUD;

/**
 * This is a template for a database configuration used by the \Holmby\CRUD classes
 */
abstract class Config {
  const PRODUCTION_SERVER = fale;

  const DB_HOST = '';//self::DB_HOST;  //'localhost'; Note, using 'localhost' makes php use sockets, not TCP/IP
  const DB_USER = '';//self::DB_USER;
  const DB_PWD = '';//self::DB_PWD;
  const DB_DATABASE = '';//self::DB_DATABASE;

  //--- Authentication ---
  const JWT_LIFETIME = '';//self::JWT_LIFETIME; // lifetime of jwt-token
  const PUBLIC_KEY = '';//self::PUBLIC_KEY;
  const PRIVATE_KEY = '';//self::PRIVATE_KEY;
}

?>
